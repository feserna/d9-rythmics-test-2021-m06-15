/**
 * @file
 * Global utilities.
 *
 */
(function ($, Drupal) {

  'use strict';

  Drupal.behaviors.bootstrap_custom = {
    attach: function (context, settings) {
    }
  };

  Drupal.behaviors.videos = {
    attach: function (context, settings) {
      videoOnClick();


    }
  };


  function videoOnClick(context) {
    $('body stop').once('.content').each(function () {   
      
     $('.node--type-video.node--view-mode-teaser img').on('click', function () {
      
      $('.node--type-video.node--view-mode-teaser img').show();
      $('.node--type-video.node--view-mode-teaser .field--name-field-embed-video').hide();

      $(this).parent().parent().find('.field--name-field-embed-video').show();
      $(this).hide();

     });

     $('.node--type-video.node--view-mode-teaser .field--name-title').on('click', function () {
      
      $('.node--type-video.node--view-mode-teaser img').show();
      $('.node--type-video.node--view-mode-teaser .field--name-field-embed-video').hide();

      $(this).closest('.node--type-video').find('.field--name-field-embed-video').show();
      $(this).closest('.node--type-video').find('img').hide();

     });

   });

    // This ensures the code is only run once:
    // $('.node--type-video.node--view-mode-teaser img', context).once('click', function () {
    //   alert('mica');
    // });
  }



})(jQuery, Drupal);
